/*
   Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
 */

///////////////////////////////////////////////////////////////////
// InDetCosmicsEventPhaseTool.cxx, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#include "InDetCosmicsEventPhase/InDetCosmicsEventPhaseTool.h"

#include "InDetRIO_OnTrack/TRT_DriftCircleOnTrack.h"

#include "TRT_ConditionsServices/ITRT_CalDbSvc.h"
#include "TRT_ConditionsData/RtRelation.h"
#include "TRT_ConditionsData/BasicRtRelation.h"

#include "InDetIdentifier/TRT_ID.h"
#include "TrkTrack/Track.h"
#include "TrkSegment/TrackSegment.h"

#include <limits>
#include <cmath>
//================ Constructor =================================================

InDet::InDetCosmicsEventPhaseTool::InDetCosmicsEventPhaseTool(std::string const& t
                                                              , std::string const& n
                                                              , IInterface const* p
                                                              ) : AthAlgTool(t, n, p)
  , m_averageT0(0.)
  , m_trtconddbsvc("TRT_CalDbSvc", n) {
  declareInterface<IInDetCosmicsEventPhaseTool>(this);
  declareProperty("TRTCalDbSvc", m_trtconddbsvc);
  declareProperty("UseTRTCalibration", m_gett0 = true);
  declareProperty("UseNewEP", m_useNewEP = true);
  declareProperty("GlobalOffset", m_globalOffset = 10.);
}

//================ Destructor =================================================

InDet::InDetCosmicsEventPhaseTool::~InDetCosmicsEventPhaseTool()
{}


//================ Initialisation =================================================

StatusCode InDet::InDetCosmicsEventPhaseTool::initialize() {
  ATH_CHECK( m_trtconddbsvc.retrieve());
  return StatusCode::SUCCESS;
}

//================ Finalisation =================================================

StatusCode InDet::InDetCosmicsEventPhaseTool::finalize() {
  return StatusCode::SUCCESS;
}

//============================================================================================

void InDet::InDetCosmicsEventPhaseTool::beginRun() {
  m_averageT0 = 0.;

  if (!m_useNewEP) {
    ATH_MSG_INFO("InDetCosmicsEventPhaseTool::beginRun, averageT0 = 0 (m_useNewEP=false) ");
    return;
  }
  TRT_ID const* TRTHelper;
  if (detStore()->retrieve(TRTHelper, "TRT_ID").isFailure()) {
    ATH_MSG_FATAL("Could not get TRT ID helper");
    return;
  }
  int countAverageT0 = 0; // should be equal to 32*2*1642  105088
  double rtShift = 0.; // shift from 0 of the RT relation
  for (std::vector<Identifier>::const_iterator it = TRTHelper->straw_layer_begin(); it != TRTHelper->straw_layer_end();
       it++) {
    int nStrawsInLayer = TRTHelper->straw_max(*it);
    for (int i = 0; i <= nStrawsInLayer; i++) {
      Identifier id = TRTHelper->straw_id(*it, i);
      if (std::abs(TRTHelper->barrel_ec(id)) != 1)                                               // average only for
                                                                                                 // barrel straws
        continue;
      m_averageT0 += m_trtconddbsvc->getT0(id); // access straw T0 same as elsewhere, countAverageT0++;
      countAverageT0++;
      const TRTCond::RtRelation* rtRelation = m_trtconddbsvc->getRtRelation(id);
      if (!rtRelation) {
        ATH_MSG_DEBUG("rtRelation missing in InDetCosmicsEventPhase::initialize!");
        continue;
      }
      rtShift += rtRelation->drifttime(0.);
    }
  }
  if (countAverageT0 != 0){
    m_averageT0 /= (double(countAverageT0));
  } else {
    m_averageT0 = std::numeric_limits<double>::infinity();
  }
  double averageT0 = m_averageT0;
  if (countAverageT0 != 0) {
    rtShift /= (double(countAverageT0));
  } else {
    rtShift = std::numeric_limits<double>::infinity();
  }

  m_averageT0 -= 20.; // shift so that the range is about the same as before
  m_averageT0 += rtShift;
  //print value for test
  ATH_MSG_DEBUG("The number of straws seen is: " << countAverageT0
                                                 << " and the values expected is:  " << 105088);
  ATH_MSG_DEBUG("The average T0 is: " << averageT0
                                      << ", average RT(r=0) is " << rtShift
                                      << " and we are subtracting: " << m_averageT0);
  ATH_MSG_INFO("InDetCosmicsEventPhaseTool::beginRun Using updated EP calculation (December 2009), subtracting: " << m_averageT0
                                                                                                                  << " ns (average T0: " << averageT0
                                                                                                                  << " ns, average RT(r=0): " << rtShift << " ns), GlobalOffset: " << m_globalOffset <<
  " ns.");

  return;
}

double InDet::InDetCosmicsEventPhaseTool::findPhase(Trk::Track const* track) const {
  ATH_MSG_DEBUG("Finding phase...");

  std::vector<float> data{0., 0.1, -0.00087,0.};
  TRTCond::RtRelation const* rtr = (m_gett0) ? 0
                                   : new TRTCond::BasicRtRelation(data);
  double timeresidualsum = 0;
  size_t ntrthits = 0;
  for (Trk::TrackStateOnSurface const* state : *track->trackStateOnSurfaces()) {
    Trk::MeasurementBase const* mesb = state->measurementOnTrack();
    if (!mesb
        || !state->type(Trk::TrackStateOnSurface::Measurement)
        ) continue;
    InDet::TRT_DriftCircleOnTrack const* trtcirc = dynamic_cast<InDet::TRT_DriftCircleOnTrack const*>(mesb);
    if (!trtcirc) continue;
    InDet::TRT_DriftCircle const* rawhit = trtcirc->prepRawData();
    if (!rawhit) continue;

    if (!rawhit->driftTimeValid()
        || rawhit->firstBinHigh()
        ) continue;
    Identifier const& ident = trtcirc->identify();
    double rawdrifttime = rawhit->rawDriftTime();
    double t0 = (m_gett0) ? m_trtconddbsvc->getT0(ident)
                : 0;
    ATH_MSG_DEBUG("T0 : " << t0);
    if (m_gett0) rtr = m_trtconddbsvc->getRtRelation(ident);
    Trk::TrackParameters const* tparp = (state->trackParameters());
    if (!tparp) continue;
    double trkdistance = tparp->parameters()[Trk::driftRadius];
    double trkdrifttime = rtr->drifttime(fabs(trkdistance));
    double timeresidual = rawdrifttime - t0 + m_averageT0 - trkdrifttime;
    ATH_MSG_DEBUG("trkdistance=" << trkdistance
                                 << "  trkdrifttime=" << trkdrifttime
                                 << "  timeresidual=" << timeresidual
                                 << " rawdrifttime=" << rawdrifttime);

    if (timeresidual < 2000 && std::fabs(trkdistance) < 2.8) {
      timeresidualsum += timeresidual;
      ++ntrthits;
    }
  }
  if (!m_gett0) delete rtr;
  ATH_MSG_DEBUG("timeresidualsum = " << timeresidualsum);
  ATH_MSG_DEBUG("ntrtrhits = " << ntrthits);
  if (ntrthits > 1) return timeresidualsum / ntrthits + m_globalOffset;
  return 0.;
}

double InDet::InDetCosmicsEventPhaseTool::findPhase(Trk::Segment const* segment) const {
  std::vector<float> data{0., 0.1, -0.00087, 0.};
  TRTCond::RtRelation const* rtr = (m_gett0) ? 0
                                   : new TRTCond::BasicRtRelation(data);
  double sum_tr = 0.;
  double sum_goodhits = 0.;
  int nhits = segment->numberOfMeasurementBases();
  for (int i = 0; i < nhits; ++i) {
    Trk::RIO_OnTrack const* rio = dynamic_cast<Trk::RIO_OnTrack const*>(segment->measurement(i));
    InDet::TRT_DriftCircleOnTrack const* trtcirc = dynamic_cast<InDet::TRT_DriftCircleOnTrack const*>(rio);
    if (!trtcirc) continue;
    InDet::TRT_DriftCircle const* rawhit = trtcirc->prepRawData();
    if (!rawhit) continue;
    if (!rawhit->lastBinHigh() && !rawhit->isNoise()) {
      Identifier const& ident = trtcirc->identify();
      if (m_gett0) rtr = m_trtconddbsvc->getRtRelation(ident);
      if (not rtr) {
        ATH_MSG_WARNING("Rt relation pointer is null!");
        return 0.;
      }
      double full_drifttime = rtr->drifttime(2.0);
      sum_tr += rawhit->trailingEdge() * 3.125 - full_drifttime;
      ATH_MSG_VERBOSE("Hit " << sum_goodhits
                             << " : " << rawhit->trailingEdge() * 3.125
                             << "  fulldrifttime=" << full_drifttime);
      sum_goodhits += 1;
    } else {
      ATH_MSG_VERBOSE("Hit has lastbin high");
    }
  }
  if (!m_gett0) delete rtr;
  if (sum_goodhits > 1) return sum_tr / sum_goodhits + m_globalOffset;
  return 0;
}

double InDet::InDetCosmicsEventPhaseTool::findPhaseFromTE(Trk::Track const* track) const {
  ATH_MSG_DEBUG("Finding phase...");

  double timeresidualsum = 0;
  size_t ntrthits = 0;

  for (Trk::TrackStateOnSurface const* state : *track->trackStateOnSurfaces()) {
    Trk::MeasurementBase const* mesb = state->measurementOnTrack();
    if (!mesb
        || !state->type(Trk::TrackStateOnSurface::Measurement)
        ) continue;
    InDet::TRT_DriftCircleOnTrack const* trtcirc = dynamic_cast<InDet::TRT_DriftCircleOnTrack const*>(mesb);
    if (!trtcirc) continue;
    InDet::TRT_DriftCircle const* rawhit = trtcirc->prepRawData();
    if (!rawhit) continue;
    Identifier const& ident = trtcirc->identify();
    double rawtrailingedge = rawhit->trailingEdge() * 3.125;
    double t0 = (m_gett0) ? m_trtconddbsvc->getT0(ident)
                : 0;
    ATH_MSG_DEBUG("T0 : " << t0);
    double timeresidual = rawtrailingedge - t0 + m_averageT0;
    ATH_MSG_DEBUG("timeresidual=" << timeresidual);
    if (timeresidual < 2000) {
      timeresidualsum += timeresidual;
      ++ntrthits;
    }
  }

  if (ntrthits > 1) return timeresidualsum / ntrthits + m_globalOffset;

  return 0.;
}
